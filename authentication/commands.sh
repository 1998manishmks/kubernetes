# Setup basic authentication on Kubernetes (Deprecated in 1.19)


# Create a file with user details locally at /tmp/users/user-details.csv


# Edit the kube-apiserver static pod configured by kubeadm to pass in the user details. 
# The file is located at /etc/kubernetes/manifests/kube-apiserver.yaml


# Modify the kube-apiserver startup options to include the basic-auth file

# apiVersion: v1
# kind: Pod
# metadata:
#   creationTimestamp: null
#   name: kube-apiserver
#   namespace: kube-system
# spec:
#   containers:
#   - command:
#     - kube-apiserver
#     - --authorization-mode=Node,RBAC
#       <content-hidden>
#     - --basic-auth-file=/tmp/users/user-details.csv


# Create the necessary roles and role bindings for these users


# Once created, you may authenticate into the kube-api server using the users credentials
curl -v -k https://localhost:6443/api/v1/pods -u "user1:password123"