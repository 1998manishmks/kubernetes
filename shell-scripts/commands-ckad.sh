# Remember, you CANNOT edit specifications of an existing POD other than the below.
#     spec.containers[*].image
#     spec.initContainers[*].image
#     spec.activeDeadlineSeconds
#     spec.tolerations

# With Deployments you can easily edit any field/property of the POD template
# Since the pod template is a child of the deployment specification,  
# with every change the deployment will automatically delete and create a new pod with the new changes.

kubectl replace --force -f filename.yaml
kubectl run nginx --image=nginx -- --color green
kubectl run nginx --image=nginx --command -- python app.py -- --color green

# docker run -e APP_COLOR=pink simple-webapp-color
# Specifying secrets -
# a. env:
#       - name: APP_COLOR
#         value: pink
        
# b. env:
#       - name: 
#         valueFrom: 
#           configMapKeyRef: 

# c. env:
#       - name: 
#         value: 
#           secretKeyRef:

#  Using config Maps-
kubectl get configmaps
kubectl describe configmaps

#     a. Create Config Maps
        # Imperative
            kubectl create configmap config_name --from-literal=APP_COLOR=blue --from-literal=APP_MOD=prod
            kubectl create configmap config_name --from-file=app_config.properties

        # Declarative
            kubectl create -f config-map.yaml # Declarative
#     b. Inject into pods
        # env:
        #   - name: 
            # valueFrom: 
            #   configMapKeyRef: config_name

kubectl get cm

kubectl create secret generic app_secret --from-literal=HOST_NAME=mysql --from-literal=APP_MOD=prod
kubectl create secret generic app_secret --from-file=app_secrets.properties

kubectl create -f secret-data.yaml
kubectl get secrets # This hides secret values
kubectl get secret app_secret -o yaml # Can be used to get secret value also
kubectl descirbe secrets

echo -n "mysql" | base64 # Convert to base64
echo -n "bXlzcWw=" | base64 --decode # Convert back to normal

# Secrets are not encrytped just encoded
ps -aux | grep kube-api
ps -aux | grep kube-api | grep "encryption-provider-config"
apt-get install etcd-client

ls /etc/kubernetes/manifests/ # Check for kube-apiserver.yaml file
crictl pods

kubectl exec pod_name -- whoami # tells us which user is this
kubectl create serviceaccount dasboard-sa # default serviceaccount is created automatically
kubectl describe secret dasboard-sa-token-sbbm # Created when we create serviceaccount before v1.24
# Now we have to create a token
kubectl create token dashboard-sa

kubectl exec -it pod_name ls /var/run/secrets/kubernetes.io/serviceaccount # secrets are mounted at this location
kubectl exec -it pod_name cat /var/run/secrets/kubernetes.io/serviceaccount/token # To view the token

# To decode the token use this or jwt.io
jq -R 'split(".") | select(lenght > 0) | .[0] .[1] | @base64d | fromjson' <<< eyJhbGciOiJ....

# By default pod requires 0.5 vCPU and 256 Mi 

kubectl taint node node_name key=value:taint-effect 
# taint-effect can be NoSchedule | PreferNoSchedule | NoExecute
kubectl descibe node kubemaster | grep Taint # master node is tainted by default and so scheduler don't place pods in master

kubectl get pods --watch
kubectl taint node node_name Taints_value- # Dash at the end indicates we need to remove the taint
kubectl label nodes node_name <label-key>=<label-value>
kubectl label node node_01 <label-key>=<label-value>


kubectl describe pod <pod-name> | grep -i events -A 10
kubectl api-resources | grep -i persistentvolumeclaim
kubectl explain pod.spec.nodeSelector
kubectl explain pod.spec | grep -i nodeselector # -i for case insensitive
kubectl annotate --help | head -30

kubectl -n elastic-stack logs kibana
kubectl -n elastic-stack exec -it app -- cat /log/app.log
kubectl delete pod --all
kubectl get pod -o yaml > pod.yaml


# -------------------- Logging and Monitoring -------------------------- #

kubectl logs -f pod_name # Containing only one container
kubectl logs -f pod_name container_name # Multi container pods

# Runs Kubelet on each node, that contains cAdvisor or containerAdvisor
minikube addons enable metrics-server # This is for minikube

# For others
git clone https://github.com/kubernetes-incubator/metrics-server.git
kubectl create -f deploy/1.8+/

# For viewing
kubectl top node
kubectl top pod

# ----------------------------------------------------------------------- #

kubectl get pods --selector app=App1
kubectl get pods --selector app=App1 --no-headers 
kubectl get pods --selector app=App1 --no-headers | wc -l
kubectl get pods --selector app=App1,bu=finance,tier=frontend

kubectl rollout history deployment nginx --revision=1
kubectl describe deployments. nginx | grep -i image

# To rollback to specific revision we will use the `--to-revision` flag.

# Default behaviour of kubernetes is to keep the containers up and running 
# so use jobs for creating containers which don't need to run again

kubectl create -f job-definition.yaml
kubectl get jobs
kubectl logs pod_name # to see logs of the job
kubectl delete job job_name
kubectl delete -f throw-dice.yaml


kubectl create ingress <ingress-name> --rule="host/path=service:port"
kubectl create ingress ingress-test --rule="wear.my-online-store.com/wear*=wear-service:80"


kubectl get svc -n kube-system 
# The kube-dns service is exposed on port 53:
kubectl get netpol

kubectl get persistentvolume
kubectl get persistentvolumeclaim
kubectl get pvc
kubectl delete pvc myclaim

kubectl exec -it webapp -- cat /log/app.log
kubectl get sc