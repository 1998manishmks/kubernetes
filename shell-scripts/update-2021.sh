curl https://my-kube-playground:6443/api/v1/pods --key admin.key --cert admin.crt --cacert ca.crt

kubectl get pods --server my-kube-playground:6443 --client-key admin.key --client-certificate admin.crt --certificate-authority ca.crt
kubectl get pods --kubeconfig config # By default kubectl uses the config placed at $HOME/.kube/config , So if you specify config there we won't need to specify config file

kubectl config view
kubectl config view --kubeconfig my-cutom-config

kubectl config use-context prod-user@production # To change the context, it also reflects in the file

curl http://localhost:6443 -k
curl http://localhost:6443/apis -k | grep "name" # Named API groups

# The above commands might not work because we might need to pass key and cert as in line 1, Or we can create proxy, which uses the crt and other details by itself
kubectl proxy
# Now use
curl http://localhost:8001 -k

kubectl get roles
kubectl get rolebindings
kubectl describe role developer
kubectl describe rolebinding devuser-developer-binding

# To check If i as a user have access to a particular API
kubectl auth can-i create deployments
kubectl auth can-i delete nodes

# Impersonate other users
kubectl auth can-i create deployments --as dev-user

# Inspect kube-apiserver file
kubectl describe pod kube-apiserver-controlplane -n kube-system

ps -aux | grep authorization
cat /etc/kubernetes/manifests/kube-apiserver.yaml
kubectl create role role-name --verb=create,list,delete --resource=pods
kubectl create rolebinding rolebinding-name --role=developer --user=dev-user

kubectl api-resources --namespaced=true

kubectl apiserver -h | grep enable-admission-plugins
cat /etc/kubernetes/manifests/kube-apiserver.yaml # Check here for config
ps -ef | grep kube-apiserver | grep admission-plugins
kubectl exec -it kube-apiserver-controlplane -n kube-system -- kube-apiserver -h | grep 'enable-admission-plugins'
grep 'enable-admission-plugins' /etc/kubernetes/manifests/kube-apiserver.yaml

kubectl convert -f <old-file> --output-version <new-api>
kubectl convert -f nginx.yaml --output-version app/v1

# In Kubernetes versions : X.Y.Z
# Where X stands for major, Y stands for minor and Z stands for patch version.

kubectl get po -n kube-system
kubectl explain job
kubectl proxy 8001&
curl localhost:8001/apis/authorization.k8s.io

cat /etc/*release* # Check OS installed
helm repo add bitnami https://charts.bitnami.com/bitnami
helm search repo joomla
helm repo list

# Install drupal helm chart from the bitnami repository.
# Release name should be bravo.
helm install bravo bitnami/drupal

# command to verify it's installation - shows packages
helm list

# Download the bitnami apache package under the /root directory.
# Note: Do not install the package. Just download it.
helm pull --untar bitnami/apache