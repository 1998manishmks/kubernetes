docker run ubuntu
docker ps
docker ps -a
# Containers are not meant to host OS, so that is why this fails
# Container is alive as long as the process inside it lives
# While running docker run, specify the command so that is overrites the specified command
docker run ubuntu sleep 5

# FROM UBUNTU
# ENTRYPOINT ["sleep"]
# Now when we run `docker run ubuntu-sleeper 10`, it starts with sleep 10
# In case of CMD, all the commands will be replaced, whereas in ENTRYPOINT it just gets appended

# FROM UBUNTU
# ENTRYPOINT ["sleep"]
# CMD ["5"]
# Here this 5 will act as a default value when nothing is passed
docker run --entrypoint sleep2.0 ubuntu-sleeper 10
# Above will replace the sleep command with sleep2.0 
docker run --name ubuntu-sleeper ubuntu-sleeper 10

docker run --user-1000 ubuntu sleep 3000 # By default docker run as root, but now we are running with userID as 1000
# Or can set as USER as node in dockerfile 
docker run --cap-add MAC_ADMIN ubuntu sleep 3000 # Gives root user of docker container MAC_ADMIN permission
docker run --cap-drop KILL ubuntu sleep 3000 # Removes the KILL permission from root user
docker run --privileged ubuntu sleep 3000 # Gives all capabilities to the root user of docker, since docker prevents its root from having all access
 
docker run ubuntu expr 2+3