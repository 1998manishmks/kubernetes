# yml contains - 
#   apiVersion
#   kind
#   metadata
#   spec

# https://kubernetes.io/docs/reference/kubectl/cheatsheet/
# https://kubernetes.io/docs/reference/kubectl/
# https://kubernetes.io/docs/reference/kubectl/conventions/
# https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/ - secrets
# https://kubernetes.io/docs/concepts/configuration/secret/#protections
# https://kubernetes.io/docs/concepts/configuration/secret/#risks
# https://developer.hashicorp.com/vault/docs/commands - vault
# https://kubernetes.io/docs/concepts/services-networking/ingress/ - ingress
# https://kubernetes.io/docs/concepts/services-networking/ingress/#path-types - path types ingress
# https://kubernetes.github.io/ingress-nginx/examples/

kubectl run nginx —imgae=nginx
kubectl run nginx —image nignx
kubectl get pods
kubectl describe pod nginx
kubectl get pods -o wide
kubectl create -f pod-definition.yml
kubectl apply -f pod.yaml
kubectl run redis --image=redis123 --dry-run -o yaml > redis.yaml
kubectl edit


kubectl get replicaset
# Increase replica number and then run below
kubectl replace -f replicaset-definition.yaml
# Or do it with this command
kubectl scale --replicas=6 -f replicaset-definition.yaml
kubectl scale --replicas=6 replicaset myapp-replicaset # Don't change the file
                          # type       name
kubectl delete replicaset myapp-replicaset # Also delete the underlying pods
kubectl describe replicaset myapp-replicaset
kubectl edit replicaset myapp-replicaset
kubectl scale replicaset myapp-replicaset --replicas=4
kubectl delete rs myapp-replicaset


kubectl create -f deployment-definition.yaml
kubectl get deployment  # this creates replicasets automatically
kubectl get rs # rs for replicasets , this indirectly creates pods
kubectl get pods
kubectl create deployment name --image=nginx --replicas=3
kubectl get deploy

kubectl get all

kubectl rollout status deployment/deployment_name
kubectl rollout history deployment/deployment_name
# DEPLOYMENT STRATERGIES - rolling update(default) recreate
# StatergyType: RollingUpdate and Recreate
kubectl apply -f deployment-definition.yaml
kubectl set image deployment/deployment_name nginx=nginx:1.9.1
kubectl set image deployment deployment_name nginx=nginx:1.9.1
kubectl rollout undo deployment/myapp-deployment

kubectl create -f deployment-definition.yaml --record

# Service types - nodeport(listens to a port and forwards the request), clusterIP(creates a virtual IP inside the cluster, to enable commumunication between different services) , LoadBalancer(creates a load balancer in supported cloud providers)
# TargetPort - port of the container 
# Port - port of the service (these ports are in viewpoint of service). It has its own clusterIp
# NodePort - Which can be used to access the server externally (30000 -  32767) 
# Service is like a virtual service inside a node
# NodePort is used when that will be accessed externally
# ClusterIP is used when that will not be accessed externally

kubectl create -f service-definition.yaml
kubectl get services
kubectl get svc
minikube service myapp-service --url
# Uses algorithm random to see which pod should get the network while using NodePort
# Algorithm: Random
# SessionAffinity: Yes

kubectl get pods, svc

kubectl run hello-minikube
kubectl cluster-info
kubectl get nodes

kubectl get pod <pod-name> -o yaml > pod-definition.yaml

# The default output format for all kubectl commands is the human-readable plain-text format.
# The -o flag allows us to output the details in several different formats.

# kubectl [command] [TYPE] [NAME] -o <output_format>
# Here are some of the commonly used formats:
# -o jsonOutput a JSON formatted API object.
# -o namePrint only the resource name and nothing else.
# -o wideOutput in the plain-text format with any additional information.
# -o yamlOutput a YAML formatted API object.


# -----------------Starting of Namespaces --------------------------
kubectl create namespace test-123 --dry-run -o json

# Can connect to other namespaces through db-service.dev.svc.cluster.local 
# cluster.local is the default domain name of kubernetes cluster
# svc is service , dev is namespace, db-service is service name

kubectl get pods --namespace=kube-system
kubectl describe namespace tss
kubectl create -f pod-definition.yml --namespace=dev

# Add the namespace is pod-definition.yaml file, in metadata so that particular namespace is always created
# Below command makes the dev namespace permanent so you don't have to use that again and again
kubectl config set-context $(kubectl config current-context) --namespace=dev

# Both are same
kubectl get pods --all-namespaces
kubectl get pods -A

kubectl create -f compute-quota.yaml
kubectl get pods -n=dev
kubectl get ns

# --dry-run: By default as soon as the command is run, the resource will be created. 
#            If you simply want to test your command , use the --dry-run=client option. 
#            This will not create the resource, instead, tell you whether the resource can be created 
#            and if your command is right.
# -o yaml: This will output the resource definition in YAML format on screen.

# Use the above two in combination to generate a resource definition file quickly, 
# that you can then modify and create resources as required, instead of creating the files from scratch.


# Create a Service named redis-service of type ClusterIP to expose pod redis on port 6379
kubectl expose pod redis --port=6379 --name redis-service --dry-run=client -o yaml #(This will automatically use the pod's labels as selectors)
kubectl create service clusterip redis --tcp=6379:6379 --dry-run=client -o yaml #(This will not use the pods labels as selectors, instead it will assume selectors as app=redis

# Create a Service named nginx of type NodePort to expose pod nginx's port 80 on port 30080 on the nodes:
kubectl expose pod nginx --port=80 --name nginx-service --type=NodePort --dry-run=client -o yaml
# (This will automatically use the pod's labels as selectors, but you cannot specify the node port. You have to generate a definition file and then add the node port in manually before creating the service with the pod.)
#                      Or
kubectl create service nodeport nginx --tcp=80:80 --node-port=30080 --dry-run=client -o yaml
# (This will not use the pods labels as selectors)

kubectl run nginx --image=nginx --labels="app=hazelcast,env=prod"
kubectl run nginx --image=nginx --port=8080 --export=true # Also creates service with ClusterIP
